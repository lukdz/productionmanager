/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Łukasz
 */
public class MaloOkno{
    public MaloOkno(CzesciLista dane){
        JFrame frame = new JFrame("Na wyczerpaniu okno");
        Container kontener = frame.getContentPane();
        GridLayout layout = new GridLayout(3, 2);
        kontener.setLayout(layout);   
        
        JLabel ilosc_etykieta = new JLabel("Minimalna ilosc w magazynie");
        kontener.add(ilosc_etykieta);
        JTextField ilosc = new JTextField("0", 20);
        kontener.add(ilosc);
        
        JButton zapisz = new JButton("OK");
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new MaloPomOkno(dane, Integer.parseInt(ilosc.getText()));
                frame.dispose();
            }
        };
        zapisz.addActionListener(zapisz_al);
        kontener.add(zapisz);
   
        frame.pack();
        frame.setVisible(true);
    }
   
}
