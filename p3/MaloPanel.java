/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 *
 * @author Łukasz
 */
public class MaloPanel extends JPanel{
    public MaloPanel(Czesc dane){
        GridLayout layout = new GridLayout(1, 2);
        this.setLayout(layout);
        
        JLabel nazwa_etykieta = new JLabel(dane.nazwa());
        this.add(nazwa_etykieta);
        
        JLabel ilosc = new JLabel(dane.ilosc());
        this.add(ilosc);
      
    }   
}
