/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Łukasz
 */
public class CzesciOkno {
    public CzesciOkno(CzesciLista dane){
        JFrame frame = new JFrame("Części");
        Container kontener = frame.getContentPane();
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));   
        
        JLabel ilosc_etykieta = new JLabel("Wybierz część do edycji");
        kontener.add(ilosc_etykieta);
        
    
        for(int i=1; i<=dane.dlugosc(); i++){
            kontener.add(new CzescPrzycisk(dane.numer(i)));
        }
   
        frame.pack();
        frame.setVisible(true);
    }
   
}
