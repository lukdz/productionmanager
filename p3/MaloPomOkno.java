/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

/**
 *
 * @author Łukasz
 */
public class MaloPomOkno {
    public MaloPomOkno(CzesciLista dane, int min){
        JFrame frame = new JFrame("Brakujące części");
        Container kontener = frame.getContentPane();
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));
        
        for(int i=1; i<=dane.dlugosc(); i++){
            if(dane.numer(i).ilosc<min){
                kontener.add(new MaloPanel(dane.numer(i)));
            }
        }
           
        frame.pack();
        frame.setVisible(true);
    }
    
}