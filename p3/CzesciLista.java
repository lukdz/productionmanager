/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
public class CzesciLista implements Serializable{     
    CzesciLista nast;
    Czesc dane;
    int dlugosc;
    CzesciLista(){}
    CzesciLista(Czesc dane, CzesciLista nast)
    {
        this.dane = dane;
        this.nast = nast;
        dlugosc = 0;
    }
    public Czesc numer(int n)
    {
        if(n==0){
            return dane;
        }
        else{
            return nast.numer(n-1);
        }
    }
    public void dodaj(Czesc dane){
        this.nast = new CzesciLista(dane, this.nast);
        dlugosc++;
    }
    public int dlugosc(){
        return dlugosc;
    } 
}
