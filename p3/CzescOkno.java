/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author Łukasz
 */
public class CzescOkno {
    public CzescOkno(Czesc dane){
        JFrame frame = new JFrame("Edycja - Czesc");
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  ////zamkniecie programu na wyjsciu
        Container kontener = frame.getContentPane();
        GridLayout layout = new GridLayout(4, 2);
        kontener.setLayout(layout);
        
        JLabel nazwa_etykieta = new JLabel("Nazwa");
        kontener.add(nazwa_etykieta);
        JTextField nazwa = new JTextField(dane.nazwa(), 10);
        kontener.add(nazwa);
        
        JLabel cena_etykieta = new JLabel("Cena");
        kontener.add(cena_etykieta);
        JTextField cena = new JTextField(dane.cena(), 10);
        kontener.add(cena);
        
        JLabel ilosc_etykieta = new JLabel("Ilosc");
        kontener.add(ilosc_etykieta);
        JTextField ilosc = new JTextField(dane.ilosc(), 10);
        kontener.add(ilosc);
        
        JButton zapisz = new JButton("Zapisz");
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                dane.edycja(nazwa.getText(), Integer.parseInt(cena.getText()), Integer.parseInt(ilosc.getText()));
            }
        };
        zapisz.addActionListener(zapisz_al);
        kontener.add(zapisz);
        
        JButton zapisz2 = new JButton("Zapisz i zamknij");
        ActionListener zapisz2_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                dane.edycja(nazwa.getText(), Integer.parseInt(cena.getText()), Integer.parseInt(ilosc.getText()));
                frame.dispose();
            }
        };
        zapisz2.addActionListener(zapisz2_al);
        kontener.add(zapisz2);
        
        frame.pack();
        frame.setVisible(true);
    }
    
}
