/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

/**
 *
 * @author Łukasz
 */
public class MagazynOkno {
    public MagazynOkno(CzesciLista dane){
        JFrame frame = new JFrame("Magazyn");
        Container kontener = frame.getContentPane();
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));
        
        for(int i=1; i<=dane.dlugosc(); i++){
            kontener.add(new MagazynPanel(dane.numer(i)));
        }
           
        frame.pack();
        frame.setVisible(true);
    }
    
}
