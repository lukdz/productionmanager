/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;


/**
 *
 * @author Łukasz
 */
class InfoOkno {
    public InfoOkno(){
        JFrame frame = new JFrame("O programie");
        Container kontener = frame.getContentPane();
        GridLayout layout = new GridLayout(3, 1);
        kontener.setLayout(layout);
        
        JLabel e1_etykieta = new JLabel("Menadżer produkcji v1.1");
        frame.add(e1_etykieta);
        JLabel e_etykieta = new JLabel("");
        frame.add(e_etykieta);
        JLabel e2_etykieta = new JLabel("Łukasz Dzwoniarek 2015");
        frame.add(e2_etykieta);

        frame.pack();
        frame.setVisible(true);
  }
    
}