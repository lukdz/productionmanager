/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
public class Produkt extends Przedmiot implements Serializable {
    String nazwa;
    public PrzedmiotLista lista;
    Produkt (String nazwa, PrzedmiotLista lista) { 
        this.nazwa = nazwa;
        this.lista = lista;
    }
    public void edycja (String nazwa) { 
        this.nazwa = nazwa;
    }
    @Override public String nazwa () { 
        return nazwa;
    }
    public PrzedmiotLista lista () { 
        return lista;
    }
    @Override public String toString(){
        return "nazwa:" + nazwa;
    }
    
}
