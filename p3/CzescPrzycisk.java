/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Łukasz
 */
public class CzescPrzycisk extends JPanel{
    CzescPrzycisk(Czesc dane){
        this.setLayout(new GridLayout(1, 3));
        
        JLabel nazwa_etykieta = new JLabel(dane.nazwa());
        this.add(nazwa_etykieta);
        
        JButton edycja = new JButton("Edycja");
        ActionListener edycja_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                CzescOkno ok1  = new CzescOkno(dane);
            }
        };
        edycja.addActionListener(edycja_al);
        this.add(edycja);
        
  
    }
    
}