/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Łukasz
 */
public class ProduktOkno {
    public ProduktOkno(Produkt dane){
        JFrame frame = new JFrame("Edycja - Produkt");
        Container kontener = frame.getContentPane();
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));
        
        JLabel nazwa_etykieta = new JLabel("Nazwa produktu");
        kontener.add(nazwa_etykieta);
        JTextField nazwa = new JTextField(dane.nazwa(), 20);
        kontener.add(nazwa);
        
        JLabel nazwap_etykieta = new JLabel("Nazwa podzespołu  " + "Ilosc");
        kontener.add(nazwap_etykieta);
        
        for(int i=1; i<=dane.lista.dlugosc(); i++){
            kontener.add(new PrzedmiotPanel(dane.lista, i));
        }
        
        JButton dodaj = new JButton("Dodaj część");
        ActionListener dodaj_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //new DodajCzesc (dane);
            }
        };
        
        JButton zapisz = new JButton("Zapisz");
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                dane.edycja(nazwa.getText());
            }
        };
        zapisz.addActionListener(zapisz_al);
        //kontener.add(zapisz);
        
        JButton zapisz2 = new JButton("Zapisz i zamknij");
        ActionListener zapisz2_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                dane.edycja(nazwa.getText());
                frame.dispose();
            }
        };
        zapisz2.addActionListener(zapisz2_al);
        kontener.add(zapisz2);
        
        frame.pack();
        frame.setVisible(true);
    }
   
}

