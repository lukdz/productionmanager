/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Łukasz
 */
public class ProgramOkno {
    public ProgramOkno(Dane dane){
        JFrame frame = new JFrame("Menadżer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  ////zamkniecie programu na wyjsciu
        Container kontener = frame.getContentPane();
        kontener.setPreferredSize(new Dimension(250, 300));
        GridLayout layout = new GridLayout(6, 2);
        kontener.setLayout(layout);
        
        JButton czesc = new JButton("Nowa część");
        ActionListener czesc_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new NowaCzesc(dane.czesci());
            }
        };
        czesc.addActionListener(czesc_al);
        kontener.add(czesc);
        
        JButton produkt = new JButton("Nowy produkt");
        ActionListener produkt_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new NowyProdukt(dane);
            }
        };
        produkt.addActionListener(produkt_al);
        kontener.add(produkt); 
        
        JButton czesci = new JButton("Części");
        ActionListener czesci_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                CzesciOkno czesci_okno = new CzesciOkno(dane.czesci());
            }
        };
        czesci.addActionListener(czesci_al);
        kontener.add(czesci);
        
        JButton produkty = new JButton("Produkty");
        ActionListener produkty_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ProduktyOkno produkty_okno = new ProduktyOkno(dane.produkty());
            }
        };
        produkty.addActionListener(produkty_al);
        kontener.add(produkty);
        
        
        
        
        JButton magazyn = new JButton("Magazyn");
        ActionListener magazyn_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                MagazynOkno magazyn_okno = new MagazynOkno(dane.czesci());
            }
        };
        magazyn.addActionListener(magazyn_al);
        kontener.add(magazyn);
        
        JButton dostawa = new JButton("Dostawa");
        ActionListener dostawa_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                DostawaOkno produkty_okno = new DostawaOkno(dane.czesci());
            }
        };
        dostawa.addActionListener(dostawa_al);
        kontener.add(dostawa); 
        
        JButton wykorzystane = new JButton("Wykorzystane");
        ActionListener wykorzystane_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new WykorzystaneOkno(dane.czesci());
            }
        };
        wykorzystane.addActionListener(wykorzystane_al);
        kontener.add(wykorzystane);  
        
        JButton malo = new JButton("Na wyczerpaniu");
        ActionListener malo_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new MaloOkno(dane.czesci());
            }
        };
        malo.addActionListener(malo_al);
        kontener.add(malo);  
        
        
        JButton pomoc = new JButton("Pomoc");
        ActionListener pomoc_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                PomocOkno pomoc_okno = new PomocOkno();
            }
        };
        pomoc.addActionListener(pomoc_al);
        kontener.add(pomoc);
        
        JButton info = new JButton("O programie");
        ActionListener info_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                InfoOkno info_okno = new InfoOkno();
            }
        };
        info.addActionListener(info_al);
        kontener.add(info);
        
        JButton z = new JButton("Zapisz");
        ActionListener z_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try {
                FileOutputStream fileOut
                    = new FileOutputStream("lista.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(dane);
                out.close();
                fileOut.close();
                System.out.println("Plik zapisany w  lista.ser");
            } catch (IOException i) {
                i.printStackTrace();
            }
            }
        };
        z.addActionListener(z_al);
        kontener.add(z);

        JButton z2 = new JButton("Zapisz i zamknij");
        ActionListener z2_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try {
                FileOutputStream fileOut
                    = new FileOutputStream("lista.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(dane);
                out.close();
                fileOut.close();
                System.out.println("Plik zapisany w  lista.ser");
                } catch (IOException i) {
                    i.printStackTrace();
                }
                System.exit(0);
            }
        };
        z2.addActionListener(z2_al);
        kontener.add(z2);
        
        frame.pack();
        frame.setVisible(true);
    }
    
}