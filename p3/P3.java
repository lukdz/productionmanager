/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Łukasz
 */
public class P3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Dane data;
        if(false){   //true tworzy bazę testową, flase wczytuje aktualną
            Czesc c1 = new Czesc("Kabel", 12, 20);
            Czesc c2 = new Czesc("Zasilacz", 5, 4); 
            CzesciLista lista = new CzesciLista();
            lista.dodaj(c1);
            lista.dodaj(c2);
        
            PrzedmiotLista prz = new PrzedmiotLista();
            prz.dodaj(c1);
            prz.dodaj(c2);   
            Produkt p1 = new Produkt("Komp", prz);
            
            ProduktyLista pl = new ProduktyLista();
            pl.dodaj(p1);
            
            Dane data1 = new Dane(lista, pl);
            data = data1;
            
            try {
                FileOutputStream fileOut
                    = new FileOutputStream("lista.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(data1);
                out.close();
                fileOut.close();
                System.out.println("Plik zapisany w  lista.ser");
            } catch (IOException i) {
                i.printStackTrace();
            }
        }
        else{
            Dane data2;
            try {
                FileInputStream fileIn = new FileInputStream("lista.ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                data2 = (Dane) in.readObject();
                in.close();
                fileIn.close();
            } catch (IOException i)
            {
                i.printStackTrace();
                return;
            }catch(ClassNotFoundException c)
            {
                System.out.println("Nie znaleziono pliku");
                c.printStackTrace();
                return;
            }
                data = data2;
        }
        
        ProgramOkno main = new ProgramOkno(data);   
        
    }
}
