/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;


/**
 *
 * @author Łukasz
 */
public class ProduktyOkno {
    public ProduktyOkno(ProduktyLista dane){
        JFrame frame = new JFrame("Produkty");
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  ////zamkniecie programu na wyjsciu
        Container kontener = frame.getContentPane();
        GridLayout layout = new GridLayout(4, 2);
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));   
        
        //JLabel nazwa_etykieta = new JLabel(Integer.toString(dane.dlugosc()));
        //kontener.add(nazwa_etykieta);
       
        for(int i=1; i<=dane.dlugosc(); i++){
            kontener.add(new ProduktPrzycisk(dane, dane.numer(i)));
        }
        
        JButton edycja = new JButton("Odśwież");
        ActionListener edycja_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new ProduktyOkno(dane);
                frame.dispose();
            }
        };
        edycja.addActionListener(edycja_al);
        frame.add(edycja);
   
        frame.pack();
        frame.setVisible(true);
    }
   
}


